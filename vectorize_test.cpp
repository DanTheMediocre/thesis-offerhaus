#include <cmath>
#include <vector>

#define NDEBUG

using namespace std;

const float A_Na = 0.05f;
const float B_Na = -0.85f;
const float A_K = 0.02f;
const float B_K = 0.06f;
const float A_l = 0.33f;
const float B_l = 0.66f;

const float Vm_init = 0.1f;
// const float n_init = 1.0f;
// const float m_init = 0.0f;
// const float h_init = 0.5f;
// const float dVmdt_init = -0.01f; 
// const float dt = 0.000125f;

// const float V_K = -0.077f;
// const float V_Na = 0.055f;
// const float V_l = -0.065f;
// const float gb_Na = 0.040f;
// const float gb_K = 0.035f;
// const float gb_l = 0.0003f;

// const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
// const float I_m = 0.1f;     // Transmembrane Current

std::vector<float> Vm (2048,Vm_init);
// std::vector<float> dVmdt (2048,0.0f);
// std::vector<float> n (2048,n_init);
// std::vector<float> m (2048,m_init);
// std::vector<float> h (2048,h_init);

std::vector<float> Vm_next (2048,Vm_init);
// std::vector<float> n_next (2048,n_init);
// std::vector<float> m_next (2048,m_init);
// std::vector<float> h_next (2048,h_init);

// int a[256];
// int b[256];
// int c[256];

// float af[256];
// float bf[256];
// float cf[256];

// void foo(void) {
//   int i;

//   for (i=0; i<2048; i++){
//     dVmdt[i] = dVmdt_init*Vm[i];
//     Vm[i] = Vm[i] + dt*dVmdt[i];
//     n[i] = n[i] + dt*(A_Na - n[i]*(A_Na+B_Na));
//     m[i] = m[i] + dt*(A_K - m[i]*(A_K+B_K));
//     h[i] = h[i] + dt*(A_l - h[i]*(A_l+B_l));
//   }
// }

void foo2(void) {
  int i;

  for (i=0; i<2048; i++){
    Vm_next[i] = expf(Vm[i]); // + dt*S_m*(I_m - gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - gb_l*(Vm[i]-V_l));
    // n[i] += n[i] + dt*(A_Na - Vm[i]*expf((A_Na+B_Na)));
    // n_next[i] = n[i] + dt*((1.0f - n[i])*(0.01f*(10.0f-Vm[i])/(expf(0.1f*(10.0f-Vm[i]))-1.0f)) - n[i]*(0.125f*expf(Vm[i]*-0.0125f)));
    // m_next[i] = m[i] + dt*((1.0f - m[i])*(0.1f*(25.0f-Vm[i])/expf(0.1f*(25.0f-Vm[i]))) - m[i]*(4.0f*expf(Vm[i]/-18.0f)));
    // h_next[i] = h[i] + dt*((1.0f - h[i])*(0.07f*expf(Vm[i]*-0.05f)) - h[i]*(1.0f/(1.0f+expf(0.1f*(30.0f-Vm[i])))));
  }
}

int main(void)
{
    foo2();
}