/*
Daan Offerhaus
Guiding autovectorization of complex models (Hodgkins-Huxley, forward euler solver)
*/

#include <cmath>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>

using namespace std;


#define COMPARTMENTS (2<<19) //Binary Mega 2^20 ~ 1E9, easier memory alignment for compiler. Only one array of COMPARTMENTS float elements can fit fully into L3 cache
#define STATES 4

#define SIZE_L1 (256*1024*sizeof(uint8_t))            //256 kB L1 cache
#define SIZE_L2 (4*SIZE_L1)                           //1.0 MB L2 cache
#define SIZE_L3 (6*SIZE_L2)                           //6.0 MB L3 cache

#define BLOCKSIZE 4096 // (SIZE_L1/(STATES*2*sizeof(float)*2))  //One calculation block sized such that the struct-of-arrays of current state + next state for BLOCKSIZE elements (compartments) fits exactly twice into L1 cache
#define QBLOCKSIZE 16384 // (SIZE_L1/(STATES*2*sizeof(float)*2))  //One calculation block sized such that the struct-of-arrays of current state + next state for BLOCKSIZE elements (compartments) fits exactly twice into L1 cache
#define NBLOCKS 256 //(COMPARTMENTS/BLOCKSIZE)

#define ALIGNED __declspec(align(64))
#define IS_ALIGNED(T,PTR,DST) T* PTR = (T*)__builtin_assume_aligned(DST,64);

#define STATE_STRUCT

#define ITERATIONS 500

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/C_m
const float I_m = 0.1f;     // Transmembrane Current

const float dt = 0.000125f;

ALIGNED float Vm1[COMPARTMENTS];
ALIGNED float n1[COMPARTMENTS];
ALIGNED float m1[COMPARTMENTS];
ALIGNED float h1[COMPARTMENTS];

ALIGNED float Vm2[COMPARTMENTS];
ALIGNED float n2[COMPARTMENTS];
ALIGNED float m2[COMPARTMENTS];
ALIGNED float h2[COMPARTMENTS];

IS_ALIGNED(float,Vm,Vm1)
IS_ALIGNED(float,Vm_next,Vm2)
IS_ALIGNED(float,n,n1)
IS_ALIGNED(float,n_next,n2)
IS_ALIGNED(float,m,m1)
IS_ALIGNED(float,m_next,m2)
IS_ALIGNED(float,h,h1)
IS_ALIGNED(float,h_next,h2)

enum 
{
    lVM = 0,
    lN = 1,
    lM = 2,
    lH = 3,
    lMAX = 4,
} StateLabels_e;

#define OFFSET_VM 0
#define OFFSET_N 4096  //OFFSET_VM + BLOCKSIZE
#define OFFSET_M 8192  //OFFSET_N + BLOCKSIZE
#define OFFSET_H 12288 //OFFSET_M + BLOCKSIZE

ALIGNED float States1[NBLOCKS][STATES][BLOCKSIZE];
ALIGNED float States2[NBLOCKS][STATES][BLOCKSIZE];

IS_ALIGNED(float,StateNow,States1)
IS_ALIGNED(float,StateNext,States2)

ALIGNED struct stateblock_t
{
    float Vm[BLOCKSIZE];
    float n[BLOCKSIZE];
    float m[BLOCKSIZE];
    float h[BLOCKSIZE];    
};

ALIGNED stateblock_t StateBlocks1[NBLOCKS];
ALIGNED stateblock_t StateBlocks2[NBLOCKS];   

IS_ALIGNED(stateblock_t,states,StateBlocks1)
IS_ALIGNED(stateblock_t,states_next,StateBlocks2)

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.2f;
const float h_init = 0.5f;

void InitArrays(void)
{
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm[i] = Vm_init;
        n[i] = n_init;
        m[i] = m_init;
        h[i] = h_init;
    }

    for(int i = 0;i < NBLOCKS;i++)
    {
        for(int j = 0;j < BLOCKSIZE;j++)
        {
            StateNow[i*QBLOCKSIZE+OFFSET_VM+j] = Vm_init;
            StateNow[i*QBLOCKSIZE+OFFSET_N+j] = n_init;
            StateNow[i*QBLOCKSIZE+OFFSET_M+j] = h_init;
            StateNow[i*QBLOCKSIZE+OFFSET_H+j] = h_init;
        }
    }
}

void copyStep(void)
{
    memcpy(Vm_next,Vm,COMPARTMENTS);
    memcpy(n_next,n,COMPARTMENTS);
    memcpy(m_next,m,COMPARTMENTS);
    memcpy(h_next,h,COMPARTMENTS);
}

void fmaStep(float* Vm_in, float* n_in, float* m_in, float* h_in, float* Vm_out,float* n_out, float* m_out, float* h_out)
{
    #pragma ivdep
    #pragma vector aligned
    #pragma omp simd
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm_out[i] = Vm_in[i] + n_in[i]*m_in[i];
        n_out[i] = n_in[i] + m_in[i]*h_in[i];
        m_out[i] = m_in[i] + h_in[i]*Vm_in[i];
        h_out[i] = h_in[i] + Vm_in[i]*n_in[i];
    }
}

void fmaStepBlock(float StateNow[NBLOCKS][STATES][BLOCKSIZE],float StateNext[NBLOCKS][STATES][BLOCKSIZE])
{

    for(int i = 0;i < NBLOCKS;i++)   //loop compartments
    {    
        #pragma ivdep
        #pragma vector aligned
        #pragma omp simd
        for(int j = 0;j < BLOCKSIZE;j++)
        {
            StateNext[i][lVM][j] = StateNow[i][lVM][j] + StateNow[i][lN][j]*StateNow[i][lM][j];
            StateNext[i][lN][j] = StateNow[i][lN][j] + StateNow[i][lM][j]*StateNow[i][lH][j];
            StateNext[i][lM][j] = StateNow[i][lM][j] + StateNow[i][lH][j]*StateNow[i][lVM][j];
            StateNext[i][lH][j] = StateNow[i][lH][j] + StateNow[i][lVM][j]*StateNow[i][lN][j];
        }
    }
}

/*
inline void IntegrateCompartment(float& __restrict__ Vm, float& __restrict__ n, float& __restrict__ m, float& __restrict__ h, float& __restrict__ Vm_next, float& __restrict__ n_next, float& __restrict__ m_next, float& __restrict__ h_next)
{
        Vm_next = Vm + dt*S_m*I_m - dt*S_m*gb_Na*n*n*n*n*(Vm-V_Na) - dt*S_m*gb_K*h*m*m*m*(Vm-V_K) - dt*S_m*gb_l*(Vm-V_l);
        n_next = n + dt*(1.0f-n)*(0.01f*(10.0f-Vm)/(expf(-0.1f*(10.0f-Vm))-1.0f)) - dt*n*(0.125f*expf(Vm*-0.0125f));
        m_next = m + dt*(1.0f-m)*(0.1f*(25.0f-Vm)/(expf(0.1f*(25.0f-Vm))+1.0f)) - dt*m*(4.0f*expf(Vm*0.0555555556f));
        h_next = h + dt*(1.0f-h)*(0.07f/expf(Vm*0.05f)) - dt*h*(1.0f/(expf(0.1f*(30.0f-Vm))+1.0f));
}

inline void IntegrateCompartmentBlock(float* Vm, float* n, float* m, float* h, float* Vm_next, float* n_next, float* m_next, float* h_next)
{
    #pragma ivdep
    #pragma vector aligned
    for(int j=0;j<BLOCKSIZE;j++)
    {
        IntegrateCompartment(Vm[j],n[j],m[j],h[j],Vm_next[j],n_next[j],m_next[j],h_next[j]);
    }
}

void eulerStepBlock(void)
{
    #pragma ivdep
    #pragma vector aligned
    for(int i = 0;i < COMPARTMENTS;i += BLOCKSIZE)   //loop compartments
    {
        IntegrateCompartmentBlock(states[i].Vm,states[i].n,states[i].m,states[i].h,states_next[i].Vm,states_next[i].n,states_next[i].m,states_next[i].h);  
    }
}

void eulerStepBlock2(void)
{
    #pragma ivdep
    #pragma vector aligned
    for(int i = 0; i < COMPARTMENTS;i++)   //loop blocks
    {
        for(int j = 0; j < BLOCKSIZE;j++)
        {
            IntegrateCompartment(states[i].Vm[j],states[i].n[j],states[i].m[j],states[i].h[j],states_next[i].Vm[j],states_next[i].n[j],states_next[i].m[j],states_next[i].h[j]);
        }
    }
}

void eulerStep(void)
{
    #pragma ivdep
    #pragma vector aligned
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        IntegrateCompartment(Vm[i],n[i],m[i],h[i],Vm_next[i],n_next[i],m_next[i],h_next[i]);
    }
}
//*/

int main(void)
{
    

    InitArrays();

    // for(int t = 0; t < ITERATIONS; t++)
    // {
    //     copyStep();

    //     swap(Vm_next,Vm);
    //     swap(n_next,n);
    //     swap(m_next,m);
    //     swap(h_next,h);
    // }

    for(int t = 0; t < ITERATIONS; t++)
    {
        fmaStep(Vm,n,m,h,Vm_next,n_next,m_next,h_next);

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < ITERATIONS; t++)
    {
        fmaStepBlock(StateNow,StateNext);

        swap(StateNow,StateNext);
    }

    // for(int t = 0; t < ITERATIONS; t++)
    // {
    //     eulerStep();

    //     swap(Vm_next,Vm);
    //     swap(n_next,n);
    //     swap(m_next,m);
    //     swap(h_next,h);
    // }

    // for(int t = 0; t < ITERATIONS; t++)
    // {
    //     eulerStepBlock();

    //     swap(states_next,states);
    // }

    // for(int t = 0; t < ITERATIONS; t++)
    // {
    //     eulerStepBlock2();

    //     swap(states_next,states);
    // }
    return 0;
}