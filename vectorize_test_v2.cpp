#include <time.h> 
#include <stdio.h> 

#define MAX (2<<16)
#define BS 64
#define ITER 100000

float A[MAX][MAX]; 
float B[MAX][MAX]; 
float C[MAX][MAX];


void add(float a[][MAX], float b[][MAX],float c[][MAX]);
void add_block(float a[][MAX], float b[][MAX],float c[][MAX]);

int main() 
{ 
  int i, j, t; 

  time_t start, elapse1, elapse2; 
  int sec,sec2; 

  //Initialize array
  for(i=0;i<MAX;i++) {
    for(j=0;j<MAX; j++) {
      C[i][j]=0.0f;
      A[i][j]=j*1.0f;
      B[i][j]=j*1.0f;
    }
  }

  start= time(NULL);
  // for(t=0; t < ITER;t++)
  // {
    add(A, B, C);
  // }
  elapse1=time(NULL);
  // for(t=0; t < ITER;t++)
  // {
    add_block(A, B, C);
  // }
  elapse2=time(NULL);
  sec = elapse1 - start;
  sec2 = elapse2 - elapse1;
  printf("Time %d, LB %d",sec,sec2); //List time taken to complete add function 
} 

void add(float a[][MAX], float b[][MAX],float c[][MAX]) 
{
  int i, j;
  #pragma ivdep
  for(i=0;i<MAX;i++) 
  {
    for(j=0; j<MAX;j++)
    {
      c[i][j] = a[i][j] + b[j][i]; //Adds two matrices
    }
  } 
}


void add_block(float a[][MAX], float b[][MAX], float c[][MAX]) 
{ 
  int i, j, ii, jj;
  #pragma ivdep
  for(i=0;i<MAX;i+=BS) 
  {
    for(j=0; j<MAX;j+=BS) 
    {
      for(ii=i; ii<i+BS; ii++) //outer loop
      {
        for(jj=j;jj<j+BS; jj++) //Array B experiences one cache miss for every iteration of outer loop
        {
          c[ii][jj] = a[ii][jj] + b[jj][ii]; //Add the two arrays
        }
      }
    }
  } 
}