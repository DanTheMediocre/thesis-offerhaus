/*
Daan Offerhaus - 16/02/2023
*/

#include <cmath>
#include <ctime>
#include <iostream>
#include <cstdlib>

using namespace std;


#define COMPARTMENTSPOW2 (1<<24)  //Binary Mega 2^20 ~ 1E9
#define COMPARTMENTS COMPARTMENTSPOW2
#define BLOCKSIZE (1<<10)
#define STATES 4
#define NBLOCKS (1<<14)

#define ALIGN64

// const float V_K = -0.077f;
// const float V_Na = 0.055f;
// const float V_l = -0.065f;
// const float gb_Na = 0.040f;
// const float gb_K = 0.035f;
// const float gb_l = 0.0003f;

// const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
// const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.2f;
const float h_init = 0.5f;

// const float dt = 0.000125f;

__declspec(align(32)) float Vm1[COMPARTMENTS];
__declspec(align(32)) float n1[COMPARTMENTS];
__declspec(align(32)) float m1[COMPARTMENTS];
__declspec(align(32)) float h1[COMPARTMENTS];
__declspec(align(32)) float Vm2[COMPARTMENTS];
__declspec(align(32)) float n2[COMPARTMENTS];
__declspec(align(32)) float m2[COMPARTMENTS];
__declspec(align(32)) float h2[COMPARTMENTS];

__declspec(align(32)) float StateBlocks1[NBLOCKS][STATES][BLOCKSIZE];   //memory structuring: NBLOCKS sequences of each state split into BLOCKSIZE elements : {Vm[BS],n[BS],m[BS],h[BS]}[NBLOCKS]
__declspec(align(32)) float StateBlocks2[NBLOCKS][STATES][BLOCKSIZE];   //two sets of struct-of-arrays state vectors, for swapping current and next iteration

__declspec(align(32)) typedef struct
{
    float Vm[BLOCKSIZE];
    float n[BLOCKSIZE];
    float m[BLOCKSIZE];
    float h[BLOCKSIZE];
} statesBlock;

__declspec(align(32)) statesBlock S1[NBLOCKS];
__declspec(align(32)) statesBlock S2[NBLOCKS];

float *Vm, *Vm_next, *n, *n_next, *m, *m_next, *h, *h_next;
statesBlock *state, *state_next;

#define lVM 0
#define lN 1  
#define lM 2  
#define lH 3

#define OFFSET_VM 0
#define OFFSET_N (OFFSET_VM + BLOCKSIZE)
#define OFFSET_M (OFFSET_N + BLOCKSIZE)
#define OFFSET_H (OFFSET_M + BLOCKSIZE)

void InitArrays(void)
{
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm1[i] = Vm_init;
        n1[i] = n_init;
        m1[i] = m_init;
        h1[i] = h_init;
    }
    
    for(int i = 0;i < NBLOCKS;i++)
    {
        for(int j = 0;j < BLOCKSIZE;j++)
        {
            S1[i].Vm[j] = Vm_init;
            S1[i].n[j] = n_init;
            S1[i].m[j] = m_init;
            S1[i].h[j] = h_init;
        }
    }

    for(int i = 0;i < NBLOCKS;i++)
    {
        for(int j = 0;j < BLOCKSIZE;j++)
        {
            StateBlocks1[i][lVM][j] = Vm_init;
            StateBlocks1[i][lN][j] = n_init;
            StateBlocks1[i][lM][j] = m_init;
            StateBlocks1[i][lH][j] = h_init;
        }
    }
}

void loopfma(void)
{
    Vm = (float*)__builtin_assume_aligned(Vm1, 32);
    Vm_next = (float*)__builtin_assume_aligned(Vm2, 32);
    n = (float*)__builtin_assume_aligned(n1, 32);
    n_next = (float*)__builtin_assume_aligned(n2, 32);
    m = (float*)__builtin_assume_aligned(m1, 32);
    m_next = (float*)__builtin_assume_aligned(m2, 32);
    h = (float*)__builtin_assume_aligned(h1, 32);
    h_next = (float*)__builtin_assume_aligned(h2, 32);
    #pragma ivdep   //__restrict__ on all accesses
    #pragma loop_count min(COMPARTMENTS), max(COMPARTMENTS), avg(COMPARTMENTS)
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        float tVm = Vm[i];                //explicit temporal access: only 4 loads and 4 stores should be needed in general
        float tn = n[i];
        float tm = m[i];
        float th = h[i];
        Vm_next[i] = tn  + tm  * th;
        n_next[i]  = tm  + th  * tVm;
        m_next[i]  = th  + tVm * tn;
        h_next[i]  = tVm + tn  * tm;
    }
}

void loopfmablock(void) //accesses the 8 SoA states in constant non-unit stride
{
    Vm = (float*)__builtin_assume_aligned(Vm1, 32);
    Vm_next = (float*)__builtin_assume_aligned(Vm2, 32);
    n = (float*)__builtin_assume_aligned(n1, 32);
    n_next = (float*)__builtin_assume_aligned(n2, 32);
    m = (float*)__builtin_assume_aligned(m1, 32);
    m_next = (float*)__builtin_assume_aligned(m2, 32);
    h = (float*)__builtin_assume_aligned(h1, 32);
    h_next = (float*)__builtin_assume_aligned(h2, 32);
    #pragma loop_count min(NBLOCKS), max(NBLOCKS), avg(NBLOCKS)
    for(int i = 0;i < COMPARTMENTS;i+=BLOCKSIZE)   //loop compartments
    {
        #pragma ivdep
        #pragma loop_count min(BLOCKSIZE), max(BLOCKSIZE), avg(BLOCKSIZE)
        for(int j=i;j<i+BLOCKSIZE;j++)
        {
            float tVm = Vm[j];
            float tn = n[j];
            float tm = m[j];
            float th = h[j];
            Vm_next[j] = tn  + tm  * th;
            n_next[j]  = tm  + th  * tVm;
            m_next[j]  = th  + tVm * tn;
            h_next[j]  = tVm + tn  * tm;
        }
    }
}


void loopfmablock2(void)    //accesses the AoSoA blocks and the states within them in unit stride
{
    state = (statesBlock*)__builtin_assume_aligned(S1, 32);
    state_next = (statesBlock*)__builtin_assume_aligned(S2, 32);
    #pragma loop_count min(NBLOCKS), max(NBLOCKS), avg(NBLOCKS)
    for(int i = 0;i<NBLOCKS;i++)   //loop compartments
    {
        #pragma ivdep
        #pragma loop_count min(BLOCKSIZE), max(BLOCKSIZE), avg(BLOCKSIZE)
        for(int j=0;j<BLOCKSIZE;j++)
        {
            float tVm = state[i].Vm[j];
            float tn = state[i].n[j];
            float tm = state[i].m[j];
            float th = state[i].h[j];
            state_next[i].Vm[j] = tn  + tm  * th;
            state_next[i].n[j]  = tm  + th  * tVm;
            state_next[i].m[j]  = th  + tVm * tn;
            state_next[i].h[j]  = tVm + tn  * tm;
        }
    }
}

void loopfmablock3(void)    //accesses the AoSoA blocks and the states within them in unit stride
{
    Vm = (float*)__builtin_assume_aligned((StateBlocks1+OFFSET_VM), 32);
    Vm_next = (float*)__builtin_assume_aligned((StateBlocks2+OFFSET_VM), 32);
    n = (float*)__builtin_assume_aligned((StateBlocks1+OFFSET_N), 32);
    n_next = (float*)__builtin_assume_aligned((StateBlocks2+OFFSET_N), 32);
    m = (float*)__builtin_assume_aligned((StateBlocks1+OFFSET_M), 32);
    m_next = (float*)__builtin_assume_aligned((StateBlocks2+OFFSET_M), 32);
    h = (float*)__builtin_assume_aligned((StateBlocks1+OFFSET_H), 32);
    h_next = (float*)__builtin_assume_aligned((StateBlocks2+OFFSET_H), 32);

    #pragma loop_count min(NBLOCKS), max(NBLOCKS), avg(NBLOCKS)
    for(int i = 0;i<COMPARTMENTS;i+=BLOCKSIZE)   //loop compartments
    {
        #pragma ivdep
        #pragma loop_count min(BLOCKSIZE), max(BLOCKSIZE), avg(BLOCKSIZE)
        for(int j=i;j<i+BLOCKSIZE;j++)
        {
            float tVm = Vm[j];
            float tn = n[j];
            float tm = m[j];
            float th = h[j];
            Vm_next[j] = tn  + tm  * th;
            n_next[j]  = tm  + th  * tVm;
            m_next[j]  = th  + tVm * tn;
            h_next[j]  = tVm + tn  * tm;
        }
    }
}


int main(void)
{
    InitArrays();
    
    loopfma();
    loopfmablock();
    loopfmablock2();
    loopfmablock3();

    return 0;
}