#include <cmath>
#include <vector>

using namespace std;

#define COMPARTMENTS 1000000
#define TIMESTEPS 500

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.0f;
const float h_init = 0.5f;

const float dt = 0.000125f;

vector<float> Vm (COMPARTMENTS,Vm_init);
vector<float> n (COMPARTMENTS,n_init);
vector<float> m (COMPARTMENTS,m_init);
vector<float> h (COMPARTMENTS,h_init);

vector<float> Vm_next (COMPARTMENTS,Vm_init);
vector<float> n_next (COMPARTMENTS,n_init);
vector<float> m_next (COMPARTMENTS,m_init);
vector<float> h_next (COMPARTMENTS,h_init);

int main(void)
{
    for(int t = 0; t < TIMESTEPS; t++)
    {
        // #pragma omp parallel simd default(shared)
        for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
        {
            Vm[i] = Vm[i] + dt*S_m*(I_m - gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - gb_l*(Vm[i]-V_l));
            n[i] = n[i] + dt*((1.0f - n[i])*(0.01f*(10.0f-Vm[i])/expm1f(0.1f*(10.0f-Vm[i]))) - n[i]*(0.125f*expf(Vm[i]*-0.0125f)));
            m[i] = m[i] + dt*((1.0f - m[i])*(0.1f*(25.0f-Vm[i])/expf(0.1f*(25.0f-Vm[i]))) - m[i]*(4.0f*expf(Vm[i]/-18.0f)));
            h[i] = h[i] + dt*((1.0f - h[i])*(0.07f*expf(Vm[i]*-0.05f)) - h[i]*(1.0f/(1.0f+expf(0.1f*(30.0f-Vm[i])))));
        }
        Vm = Vm_next;
        n = n_next;
        m = m_next;
        h = h_next;
    }
}