//function expansion of the Hodgkins Huxley model, splitting it into single operands, number indicating order of operations depth (all depth n+1 are dependent on some variable from depth n)
HH_foo()
{
//    Vm = Vm + dt*dn(Vm,n,m,h);
1    gNa1 = -gb_Na*n;
2    gNa2 = gNa1*n;
3    gNa3 = gNa2*n;
4    gNa =  gNa3*n
    
1    gK1 = -gb_K*m;
2    gK2 = gK1*m;
3    gK3 = gK2*m;
4    gK = gK3*h;

1    VmNa = Vm-V_Na;
1    VmK = Vm-V_K;
1    Vml = Vm-V_l;
    
5    dVNa = gNa*VmNa;
5    dVK = gK*VmK;
2    dVl = -gb_l*Vml;

1    SIm = Sm*Im; //optional, may be const
6    dVm1 = SIm+Sm*dVNa;
7    dVm2 = dVm1+Sm*dVK;
8    dVm = dVm2+Sm*dVl;
9    Vm_next = Vm+dt*dVm;
    
//    n = n + dt*dn(Vm,n);
1    na1 = 0.1f+(-0.01f)*Vm;
1    na2 = (-1.0f)+0.1f*Vm;
1    nb = (-0.0125f)*Vm;

2    nea1 = expf(na2); //expf high cycle count, but absolutely necessary
3    nea2 = nea1 + (-1.0f);
4    nf1 = na1/nea2;        //div high cycle count, annoying (not easy to expand (e^x - 1)^-1 to turn into * instead)
    
2    neb1 = expf(nb);
3    neb2 = -0.125f*neb1;
5    nf2 = neb2*nf1;
    
6    dn = nf1 + n*nf2;
7    n_next = n + dt*dn;

//    m = m + dt*dm(Vm,m);
1    ma1 = 2.5f+(-0.1f)*Vm;
1    mb = (-0.0555555556f)*Vm;

2    mea1 = expf(ma1); //expf high cycle count, but absolutely necessary
3    mea2 = mea1 + (-1.0f);
4    mf1 = ma1/mea2;        //div high cycle count, annoying (not easy to expand (e^x - 1)^-1 to turn into * instead)
    
2    meb1 = expf(mb);
3    meb2 = -4.0f*meb1;
5    mf2 = meb2*mf1;
    
6    dm = mf1 + m*mf2;
7    m_next = m + dt*dm;
    
//    h = h + dt*dh(Vm,h);
1    ha1 = 0.05f*Vm;
1    hb1 = 3.0f+(-0.1f)*Vm;
    
2    hea1 = expf(ha1);
3    hf1 = 0.07f*hea1;

2    heb1 = expf(ha1);
3    heb2 = hea1 + 1.0f;
4    hf2 = hf1/heb1;

5    dh = hf1 + h*hf2;
6    h_next = h + dt*dh;
} //total 6 add/sub, 20 mul, 12 FMA, 3 div, 6 expf.