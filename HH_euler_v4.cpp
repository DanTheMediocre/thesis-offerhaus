#include <cmath>
#include <vector>
#include <ctime>
#include <iostream>

using namespace std;

#define COMPARTMENTS 1000000
#define TIMESTEPS 1000

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.0f;
const float h_init = 0.5f;

const float dt = 0.000125f;

float Vm1[COMPARTMENTS];
float n1[COMPARTMENTS];
float m1[COMPARTMENTS];
float h1[COMPARTMENTS];

float Vm2[COMPARTMENTS];
float n2[COMPARTMENTS];
float m2[COMPARTMENTS];
float h2[COMPARTMENTS];

float* Vm = &Vm1[0];
float* Vm_next = &Vm2[0];
float* n = &n1[0];
float* n_next = &n2[0];
float* m = &m1[0];
float* m_next = &m2[0];
float* h = &h1[0];
float* h_next = &h2[0];

void initArrays(void)
{
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm[i] = Vm_init;
        n[i] = n_init;
        m[i] = m_init;
        h[i] = h_init;
    }
}

void eulerStep(void)
{
    #pragma ivdep
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {

        Vm_next[i] = Vm[i] + dt*S_m*I_m - dt*S_m*gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - dt*S_m*gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - dt*S_m*gb_l*(Vm[i]-V_l);
        n_next[i] = n[i] + dt*(1.0f-n[i])*(0.01f*(10.0f-Vm[i])/(expf(-0.1f*(10.0f-Vm[i]))-1.0f)) - dt*n[i]*(0.125f*expf(Vm[i]*-0.0125f));
        m_next[i] = m[i] + dt*(1.0f-m[i])*(0.1f*(25.0f-Vm[i])/(expf(0.1f*(25.0f-Vm[i]))+1.0f)) - dt*m[i]*(4.0f*expf(Vm[i]*0.0555555556f));
        h_next[i] = h[i] + dt*(1.0f-h[i])*(0.07f/expf(Vm[i]*0.05f)) - dt*h[i]*(1.0f/(expf(0.1f*(30.0f-Vm[i]))+1.0f));
    
    }
}

int main(void)
{
    initArrays();
    for(int t = 0; t < TIMESTEPS; t++)
    {
        //do one timestep for all compartments (globally defined so no argument passing)
        eulerStep();
        //swap buffers
        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    return 0;
}