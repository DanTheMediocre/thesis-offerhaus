/*
Daan Offerhaus - 16/02/2023
*/

#include <cmath>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>

using namespace std;


#define COMPARTMENTSPOW2 2<<19  //Binary Mega 2^20 ~ 1E9, easier memory alignment for compiler
#define COMPARTMENTS 1000000
#define BLOCKSIZE 8

// #define ALIGN64

#define TIMESTEPS 1000

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.0f;
const float h_init = 0.5f;

const float dt = 0.000125f;

#ifdef ALIGN64
__declspec(align(64)) float Vm1[COMPARTMENTS];
__declspec(align(64)) float n1[COMPARTMENTS];
__declspec(align(64)) float m1[COMPARTMENTS];
__declspec(align(64)) float h1[COMPARTMENTS];

__declspec(align(64)) float Vm2[COMPARTMENTS];
__declspec(align(64)) float n2[COMPARTMENTS];
__declspec(align(64)) float m2[COMPARTMENTS];
__declspec(align(64)) float h2[COMPARTMENTS];

float *Vm, *Vm_next, *n, *n_next, *m, *m_next, *h, *h_next;
#else
float Vm1[COMPARTMENTS];
float n1[COMPARTMENTS];
float m1[COMPARTMENTS];
float h1[COMPARTMENTS];

float Vm2[COMPARTMENTS];
float n2[COMPARTMENTS];
float m2[COMPARTMENTS];
float h2[COMPARTMENTS];

float* Vm = &Vm1[0];
float* Vm_next = &Vm2[0];
float* n = &n1[0];
float* n_next = &n2[0];
float* m = &m1[0];
float* m_next = &m2[0];
float* h = &h1[0];
float* h_next = &h2[0];
#endif

void InitArrays(void)
{
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm[i] = Vm_init;
        n[i] = n_init;
        m[i] = m_init;
        h[i] = h_init;
    }
}

void copyStep(void)
{
    memcpy(Vm_next,Vm,COMPARTMENTS);
    memcpy(n_next,n,COMPARTMENTS);
    memcpy(m_next,m,COMPARTMENTS);
    memcpy(h_next,h,COMPARTMENTS);
}

void copyStep2(void)
{
    memcpy(Vm_next                  ,Vm ,COMPARTMENTS/4);
    memcpy(Vm_next+COMPARTMENTS/4   ,n  ,COMPARTMENTS/4);
    memcpy(Vm_next+2*COMPARTMENTS/4 ,m  ,COMPARTMENTS/4);
    memcpy(Vm_next+3*COMPARTMENTS/4 ,h  ,COMPARTMENTS/4);

    memcpy(n_next                   ,Vm+COMPARTMENTS/4  ,COMPARTMENTS/4);
    memcpy(n_next+COMPARTMENTS/4    ,n+COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(n_next+2*COMPARTMENTS/4  ,m+COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(n_next+3*COMPARTMENTS/4  ,h+COMPARTMENTS/4   ,COMPARTMENTS/4);

    memcpy(m_next                   ,Vm+2*COMPARTMENTS/4  ,COMPARTMENTS/4);
    memcpy(m_next+COMPARTMENTS/4    ,n+2*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(m_next+2*COMPARTMENTS/4  ,m+2*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(m_next+3*COMPARTMENTS/4  ,h+2*COMPARTMENTS/4   ,COMPARTMENTS/4);

    memcpy(h_next                   ,Vm+3*COMPARTMENTS/4  ,COMPARTMENTS/4);
    memcpy(h_next+COMPARTMENTS/4    ,n+3*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(h_next+2*COMPARTMENTS/4  ,m+3*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(h_next+3*COMPARTMENTS/4  ,h+3*COMPARTMENTS/4   ,COMPARTMENTS/4);
}


inline void IntegrateCompartmentBlock(float* Vm, float* n, float* m, float* h, float* Vm_next, float* n_next, float* m_next, float* h_next)
{    
    #pragma ivdep
    #pragma unroll BLOCKSIZE
    for(int j=0;j<BLOCKSIZE;j++)
    {
        Vm_next[j] = Vm[j] + dt*S_m*I_m - dt*S_m*gb_Na*n[j]*n[j]*n[j]*n[j]*(Vm[j]-V_Na) - dt*S_m*gb_K*h[j]*m[j]*m[j]*m[j]*(Vm[j]-V_K) - dt*S_m*gb_l*(Vm[j]-V_l);
        n_next[j] = n[j] + dt*(1.0f-n[j])*(0.01f*(10.0f-Vm[j])/(expf(-0.1f*(10.0f-Vm[j]))-1.0f)) - dt*n[j]*(0.125f*expf(Vm[j]*-0.0125f));
        m_next[j] = m[j] + dt*(1.0f-m[j])*(0.1f*(25.0f-Vm[j])/(expf(0.1f*(25.0f-Vm[j]))+1.0f)) - dt*m[j]*(4.0f*expf(Vm[j]*0.0555555556f));
        h_next[j] = h[j] + dt*(1.0f-h[j])*(0.07f/expf(Vm[j]*0.05f)) - dt*h[j]*(1.0f/(expf(0.1f*(30.0f-Vm[j]))+1.0f));
    }
}

void eulerStepBlock(void)
{
    #pragma ivdep
    for(int i = 0;i < COMPARTMENTS;i += BLOCKSIZE)   //loop compartments
    {
        IntegrateCompartmentBlock(Vm+i,n+i,m+i,h+i,Vm_next+i,n_next+i,m_next+i,h_next+i);
    }
}

void eulerStep(void)
{
    #pragma ivdep   //__restrict__ on all accesses
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm_next[i] = Vm[i] + dt*S_m*I_m - dt*S_m*gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - dt*S_m*gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - dt*S_m*gb_l*(Vm[i]-V_l);
        n_next[i] = n[i] + dt*(1.0f-n[i])*(0.01f*(10.0f-Vm[i])/(expf(-0.1f*(10.0f-Vm[i]))-1.0f)) - dt*n[i]*(0.125f*expf(Vm[i]*-0.0125f));
        m_next[i] = m[i] + dt*(1.0f-m[i])*(0.1f*(25.0f-Vm[i])/(expf(0.1f*(25.0f-Vm[i]))+1.0f)) - dt*m[i]*(4.0f*expf(Vm[i]*0.0555555556f));
        h_next[i] = h[i] + dt*(1.0f-h[i])*(0.07f/expf(Vm[i]*0.05f)) - dt*h[i]*(1.0f/(expf(0.1f*(30.0f-Vm[i]))+1.0f));
    }
}

int main(void)
{
    #ifdef ALIGN64
    Vm = (float*)__builtin_assume_aligned(Vm1, 64);
    Vm_next = (float*)__builtin_assume_aligned(Vm2, 64);
    n = (float*)__builtin_assume_aligned(n1, 64);
    n_next = (float*)__builtin_assume_aligned(n2, 64);
    m = (float*)__builtin_assume_aligned(m1, 64);
    m_next = (float*)__builtin_assume_aligned(m2, 64);
    h = (float*)__builtin_assume_aligned(h1, 64);
    h_next = (float*)__builtin_assume_aligned(h2, 64);
    #endif

    InitArrays();

    for(int t = 0; t < TIMESTEPS/4; t++)
    {
        copyStep();
    
        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/4; t++)
    {
        copyStep2();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/4; t++)
    {
        eulerStep();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/4; t++)
    {
        eulerStepBlock();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }
    return 0;
}