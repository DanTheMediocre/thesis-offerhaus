/*
Daan Offerhaus - 16/02/2023
*/

#include <cmath>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>

using namespace std;


#define COMPARTMENTSPOW2 2<<19  //Binary Mega 2^20 ~ 1E9, easier memory alignment for compiler
#define COMPARTMENTS 1000000
#define BLOCKSIZE 4096
#define STATES 4
#define NBLOCKS 256

#define ALIGN64
// #define PREFETCH
#define TIMESTEPS 1000

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.2f;
const float h_init = 0.5f;

const float dt = 0.000125f;


#ifdef ALIGN64
__declspec(align(64)) float Vm1[COMPARTMENTS];
__declspec(align(64)) float n1[COMPARTMENTS];
__declspec(align(64)) float m1[COMPARTMENTS];
__declspec(align(64)) float h1[COMPARTMENTS];

__declspec(align(64)) float Vm2[COMPARTMENTS];
__declspec(align(64)) float n2[COMPARTMENTS];
__declspec(align(64)) float m2[COMPARTMENTS];
__declspec(align(64)) float h2[COMPARTMENTS];

// __declspec(align(64)) float StateBlocks1[NBLOCKS][STATES][BLOCKSIZE];   //memory structuring: NBLOCKS sequences of each state split into BLOCKSIZE elements : {Vm[BS],n[BS],m[BS],h[BS]}[NBLOCKS]
// __declspec(align(64)) float StateBlocks2[NBLOCKS][STATES][BLOCKSIZE];   //two sets of struct-of-arrays state vectors, for swapping current and next iteration

union {__declspec(align(64)) float lin[COMPARTMENTS*STATES];__declspec(align(64)) float dim[NBLOCKS][STATES][BLOCKSIZE];} StateBlocks1;
union {__declspec(align(64)) float lin[COMPARTMENTS*STATES];__declspec(align(64)) float dim[NBLOCKS][STATES][BLOCKSIZE];} StateBlocks2;

enum 
{
    lVM = 0,
    lN = 1,
    lM = 2,
    lH = 3,
    lMAX = 4,
} StateLabels_e;

float *Vm, *Vm_next, *n, *n_next, *m, *m_next, *h, *h_next, *state, *state_next;

#define OFFSET_VM 0
#define OFFSET_N 4096  //OFFSET_VM + BLOCKSIZE
#define OFFSET_M 8192  //OFFSET_N + BLOCKSIZE
#define OFFSET_H 12288 //OFFSET_M + BLOCKSIZE

#else
float Vm1[COMPARTMENTS];
float n1[COMPARTMENTS];
float m1[COMPARTMENTS];
float h1[COMPARTMENTS];

float Vm2[COMPARTMENTS];
float n2[COMPARTMENTS];
float m2[COMPARTMENTS];
float h2[COMPARTMENTS];

float* Vm = &Vm1[0];
float* Vm_next = &Vm2[0];
float* n = &n1[0];
float* n_next = &n2[0];
float* m = &m1[0];
float* m_next = &m2[0];
float* h = &h1[0];
float* h_next = &h2[0];
#endif

void InitArrays(void)
{
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm[i] = Vm_init;
        n[i] = n_init;
        m[i] = m_init;
        h[i] = h_init;
    }
}

void copyStep(void)
{
    memcpy(Vm_next,Vm,COMPARTMENTS);
    memcpy(n_next,n,COMPARTMENTS);
    memcpy(m_next,m,COMPARTMENTS);
    memcpy(h_next,h,COMPARTMENTS);
}

void copyShuffle(void)
{
    memcpy(Vm_next                  ,Vm ,COMPARTMENTS/4);
    memcpy(Vm_next+COMPARTMENTS/4   ,n  ,COMPARTMENTS/4);
    memcpy(Vm_next+2*COMPARTMENTS/4 ,m  ,COMPARTMENTS/4);
    memcpy(Vm_next+3*COMPARTMENTS/4 ,h  ,COMPARTMENTS/4);

    memcpy(n_next                   ,Vm+COMPARTMENTS/4  ,COMPARTMENTS/4);
    memcpy(n_next+COMPARTMENTS/4    ,n+COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(n_next+2*COMPARTMENTS/4  ,m+COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(n_next+3*COMPARTMENTS/4  ,h+COMPARTMENTS/4   ,COMPARTMENTS/4);

    memcpy(m_next                   ,Vm+2*COMPARTMENTS/4  ,COMPARTMENTS/4);
    memcpy(m_next+COMPARTMENTS/4    ,n+2*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(m_next+2*COMPARTMENTS/4  ,m+2*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(m_next+3*COMPARTMENTS/4  ,h+2*COMPARTMENTS/4   ,COMPARTMENTS/4);

    memcpy(h_next                   ,Vm+3*COMPARTMENTS/4  ,COMPARTMENTS/4);
    memcpy(h_next+COMPARTMENTS/4    ,n+3*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(h_next+2*COMPARTMENTS/4  ,m+3*COMPARTMENTS/4   ,COMPARTMENTS/4);
    memcpy(h_next+3*COMPARTMENTS/4  ,h+3*COMPARTMENTS/4   ,COMPARTMENTS/4);
}

#if 0  //function expansion of the HH model, splitting it into single operands, number indicating order of operations depth (all depth n+1 are dependent on some variable from depth n)
{
//    Vm = Vm + dt*dn(Vm,n,m,h);
1    gNa1 = -gb_Na*n;
2    gNa2 = gNa1*n;
3    gNa3 = gNa2*n;
4    gNa =  gNa3*n
    
1    gK1 = -gb_K*m;
2    gK2 = gK1*m;
3    gK3 = gK2*m;
4    gK = gK3*h;

1    VmNa = Vm-V_Na;
1    VmK = Vm-V_K;
1    Vml = Vm-V_l;
    
5    dVNa = gNa*VmNa;
5    dVK = gK*VmK;
2    dVl = -gb_l*Vml;

1    SIm = Sm*Im; //optional, may be const
6    dVm1 = SIm+Sm*dVNa;
7    dVm2 = dVm1+Sm*dVK;
8    dVm = dVm2+Sm*dVl;
9    Vm_next = Vm+dt*dVm;
    
//    n = n + dt*dn(Vm,n);
1    na1 = 0.1f+(-0.01f)*Vm;
1    na2 = (-1.0f)+0.1f*Vm;
1    nb = (-0.0125f)*Vm;

2    nea1 = expf(na2); //expf high cycle count, but absolutely necessary
3    nea2 = nea1 + (-1.0f);
4    nf1 = na1/nea2;        //div high cycle count, annoying (not easy to expand (e^x - 1)^-1 to turn into * instead)
    
2    neb1 = expf(nb);
3    neb2 = -0.125f*neb1;
5    nf2 = neb2*nf1;
    
6    dn = nf1 + n*nf2;
7    n_next = n + dt*dn;

//    m = m + dt*dm(Vm,m);
1    ma1 = 2.5f+(-0.1f)*Vm;
1    mb = (-0.0555555556f)*Vm;

2    mea1 = expf(ma1); //expf high cycle count, but absolutely necessary
3    mea2 = mea1 + (-1.0f);
4    mf1 = ma1/mea2;        //div high cycle count, annoying (not easy to expand (e^x - 1)^-1 to turn into * instead)
    
2    meb1 = expf(mb);
3    meb2 = -4.0f*meb1;
5    mf2 = meb2*mf1;
    
6    dm = mf1 + m*mf2;
7    m_next = m + dt*dm;
    
//    h = h + dt*dh(Vm,h);
1    ha1 = 0.05f*Vm;
1    hb1 = 3.0f+(-0.1f)*Vm;
    
2    hea1 = expf(ha1);
3    hf1 = 0.07f*hea1;

2    heb1 = expf(ha1);
3    heb2 = hea1 + 1.0f;
4    hf2 = hf1/heb1;

5    dh = hf1 + h*hf2;
6    h_next = h + dt*dh;
}
#endif //total 6 add/sub, 20 mul, 12 FMA, 3 div, 6 expf.

inline void IntegrateCompartmentBlock(float* Vm, float* n, float* m, float* h, float* Vm_next, float* n_next, float* m_next, float* h_next)
{    
    #pragma ivdep
    for(int j=0;j<BLOCKSIZE;j++)
    {
        Vm_next[j] = Vm[j] + dt*S_m*I_m - dt*S_m*gb_Na*n[j]*n[j]*n[j]*n[j]*(Vm[j]-V_Na) - dt*S_m*gb_K*h[j]*m[j]*m[j]*m[j]*(Vm[j]-V_K) - dt*S_m*gb_l*(Vm[j]-V_l);
        n_next[j] = n[j] + dt*(1.0f-n[j])*(0.01f*(10.0f-Vm[j])/(expf(-0.1f*(10.0f-Vm[j]))-1.0f)) - dt*n[j]*(0.125f*expf(Vm[j]*-0.0125f));
        m_next[j] = m[j] + dt*(1.0f-m[j])*(0.1f*(25.0f-Vm[j])/(expf(0.1f*(25.0f-Vm[j]))+1.0f)) - dt*m[j]*(4.0f*expf(Vm[j]*0.0555555556f));
        h_next[j] = h[j] + dt*(1.0f-h[j])*(0.07f/expf(Vm[j]*0.05f)) - dt*h[j]*(1.0f/(expf(0.1f*(30.0f-Vm[j]))+1.0f));
    }
}

void eulerStepBlock(void)
{
    #pragma ivdep
    for(int i = 0;i < COMPARTMENTS;i += BLOCKSIZE)   //loop compartments
    {
        IntegrateCompartmentBlock(Vm+i,n+i,m+i,h+i,Vm_next+i,n_next+i,m_next+i,h_next+i);
    }
}

void eulerStep(void)
{
    #pragma ivdep   //__restrict__ on all accesses
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm_next[i] = Vm[i] + dt*S_m*I_m - dt*S_m*gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - dt*S_m*gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - dt*S_m*gb_l*(Vm[i]-V_l);
        n_next[i] = n[i] + dt*(1.0f-n[i])*(0.01f*(10.0f-Vm[i])/(expf(-0.1f*(10.0f-Vm[i]))-1.0f)) - dt*n[i]*(0.125f*expf(Vm[i]*-0.0125f));
        m_next[i] = m[i] + dt*(1.0f-m[i])*(0.1f*(25.0f-Vm[i])/(expf(0.1f*(25.0f-Vm[i]))+1.0f)) - dt*m[i]*(4.0f*expf(Vm[i]*0.0555555556f));
        h_next[i] = h[i] + dt*(1.0f-h[i])*(0.07f/expf(Vm[i]*0.05f)) - dt*h[i]*(1.0f/(expf(0.1f*(30.0f-Vm[i]))+1.0f));
    }
}

void loopcpy(void)
{
    #pragma ivdep
    #ifdef PREFETCH
    #pragma prefetch *:2
    #endif
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm_next[i] = n[i];
        n_next[i] = m[i];
        m_next[i] = h[i];
        h_next[i] = Vm[i];
    }
}

void loopfma(void)
{
    #pragma ivdep
    #ifdef PREFETCH
    #pragma prefetch *:2
    #endif
    for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
    {
        Vm_next[i] = n[i] + m[i]*h[i];
        n_next[i] = m[i] + h[i]*Vm[i];
        m_next[i] = h[i] + Vm[i]*n[i];
        h_next[i] = Vm[i] + n[i]*m[i];
    }
}

inline void copyBlock(float* Vm, float* n, float* m, float* h, float* Vm_next, float* n_next, float* m_next, float* h_next)
{    
    #pragma ivdep
    for(int j=0;j<BLOCKSIZE;j++)
    {
        Vm_next[j] = Vm[j];
        n_next[j] = n[j];
        m_next[j] = m[j];
        h_next[j] = h[j];
    }
}

void loopcpyblock(void)
{
    #pragma ivdep
    #ifdef PREFETCH
    #pragma prefetch *:2
    #endif
    for(int i = 0;i < COMPARTMENTS;i+=BLOCKSIZE)   //loop compartments
    {
        copyBlock(Vm+i,n+i,m+i,h+i,Vm_next+i,n_next+i,m_next+i,h_next+i);
    }
}

inline void fmaBlock(float* Vm, float* n, float* m, float* h, float* Vm_next, float* n_next, float* m_next, float* h_next)
{    
    #pragma ivdep
    for(int j=0;j<BLOCKSIZE;j++)
    {
        Vm_next[j] = n[j] + m[j]*h[j];
        n_next[j] = m[j] + h[j]*Vm[j];
        m_next[j] = h[j] + Vm[j]*n[j];
        h_next[j] = Vm[j] + n[j]*m[j];
    }
}


void loopfmablock(void)
{
    #pragma ivdep
    #ifdef PREFETCH
    #pragma prefetch *:2
    #endif
    for(int i = 0;i < COMPARTMENTS;i+=BLOCKSIZE)   //loop compartments
    {
        fmaBlock(Vm+i,n+i,m+i,h+i,Vm_next+i,n_next+i,m_next+i,h_next+i);
    }
}


int main(void)
{
    #ifdef ALIGN64
    Vm = (float*)__builtin_assume_aligned(Vm1, 64);
    Vm_next = (float*)__builtin_assume_aligned(Vm2, 64);
    n = (float*)__builtin_assume_aligned(n1, 64);
    n_next = (float*)__builtin_assume_aligned(n2, 64);
    m = (float*)__builtin_assume_aligned(m1, 64);
    m_next = (float*)__builtin_assume_aligned(m2, 64);
    h = (float*)__builtin_assume_aligned(h1, 64);
    h_next = (float*)__builtin_assume_aligned(h2, 64);
    #endif

    InitArrays();

    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        copyStep();
    
        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        copyShuffle();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        loopcpy();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }
    
    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        loopcpyblock();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        loopfma();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }
    
    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        loopfmablock();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        eulerStep();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }

    for(int t = 0; t < TIMESTEPS/8; t++)
    {
        eulerStepBlock();

        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);
    }
    return 0;
}