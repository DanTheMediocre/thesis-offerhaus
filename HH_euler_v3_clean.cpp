#include <cmath>
#include <vector>
#include <ctime>
#include <iostream>

using namespace std;

#define COMPARTMENTS 1000000
#define TIMESTEPS 1000
// #define USE_VECTOR

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.0f;
const float h_init = 0.5f;

const float dt = 0.000125f;
#ifdef USE_VECTOR
vector<float> Vm (COMPARTMENTS,Vm_init);
vector<float> n (COMPARTMENTS,n_init);
vector<float> m (COMPARTMENTS,m_init);
vector<float> h (COMPARTMENTS,h_init);

vector<float> Vm_next (COMPARTMENTS);
vector<float> n_next (COMPARTMENTS);
vector<float> m_next (COMPARTMENTS);
vector<float> h_next (COMPARTMENTS);

#else
float Vm1[COMPARTMENTS];
float n1[COMPARTMENTS];
float m1[COMPARTMENTS];
float h1[COMPARTMENTS];

float Vm2[COMPARTMENTS];
float n2[COMPARTMENTS];
float m2[COMPARTMENTS];
float h2[COMPARTMENTS];

float* Vm = &Vm1[0];
float* Vm_next = &Vm2[0];
float* n = &n1[0];
float* n_next = &n2[0];
float* m = &m1[0];
float* m_next = &m2[0];
float* h = &h1[0];
float* h_next = &h2[0];
#endif

inline float An(float& Vm)
{
    return 0.01f*(10.0f-Vm)/(expf(-0.1f*(10.0f-Vm))-1.0f);
}               

inline float Bn(float& Vm)
{
    return 0.125f*expf(Vm*-0.0125f);
}

inline float Am(float& Vm)
{
    return 0.1f*(25.0f-Vm)/(expf(0.1f*(25.0f-Vm))+1.0f);
}

inline float Bm(float& Vm)
{
    return 4.0f*expf(Vm/-18.0f);
}

inline float Ah(float& Vm)
{
    return 0.07f/expf(Vm*0.05f);
}

inline float Bh(float& Vm)
{
    return 1.0f/(expf(0.1f*(30.0f-Vm))+1.0f);
}

inline float G_Na(float& n)
{
    return  gb_Na*n*n*n*n;
}

inline float G_K(float& m, float& h)
{
    return gb_K*h*m*m*m;
}

inline float expf2(float& x)
{
    #ifdef USE_FASTEXP
    return powf((1 + x/1024.0f),1024.0f);
    #else    
    return expf(x);
    #endif
}

int main(void)
{
    #ifndef USE_VECTOR
    for(int j=0;j<COMPARTMENTS;j++)
    {
        // Vm[j] = Vm_init;
        n[j] = n_init;
        m[j] = m_init;
        h[j] = h_init;
    }
    #endif
    auto tick = clock();
    for(int t = 0; t < TIMESTEPS; t++)
    {
        #ifdef USE_VECTOR
        #pragma omp parallel for simd schedule(static) default(shared)
        for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
        {
            Vm_next[i] = Vm[i] + dt*S_m*(I_m - G_Na(n[i])*(Vm[i]-V_Na) - G_K(m[i],h[i])*(Vm[i]-V_K) - gb_l*(Vm[i]-V_l));
            n_next[i] = n[i] + dt*((1.0f - n[i])*An(Vm[i]) - n[i]*Bn(Vm[i]));
            m_next[i] = m[i] + dt*((1.0f - m[i])*Am(Vm[i]) - m[i]*Bm(Vm[i]));
            h_next[i] = h[i] + dt*((1.0f - h[i])*Ah(Vm[i]) - h[i]*Bh(Vm[i]));
        }
        //swap buffers
        Vm.swap(Vm_next);
        n.swap(n_next);
        m.swap(m_next);
        h.swap(h_next);
        #else
        // #pragma omp parallel for schedule(static) default(shared)
        // #pragma omp simd
        for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
        {
            // Vm_next[i] = Vm[i] + dt*S_m*I_m - dt*S_m*G_Na(n[i])*(Vm[i]-V_Na) - dt*S_m*G_K(h[i],m[i])*(Vm[i]-V_K) - dt*S_m*gb_l*(Vm[i]-V_l);
            // n_next[i] = n[i] + dt - dt*n[i]*An(Vm[i]) - dt*n[i]*Bn(Vm[i]);
            // m_next[i] = m[i] + dt - dt*m[i]*Am(Vm[i]) - dt*m[i]*Bm(Vm[i]);
            // h_next[i] = h[i] + dt - dt*h[i]*Ah(Vm[i]) - dt*h[i]*Bh(Vm[i]);
            
            Vm_next[i] = Vm[i] + dt*S_m*(I_m - gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - gb_K*m[i]*m[i]*m[i]*h[i]*Vm[i]);
            n_next[i] = n[i] + dt - dt*n[i]*Vm[i]*0.00001f;
            m_next[i] = m[i] + dt - dt*m[i]*Vm[i]*0.00003f;
            h_next[i] = h[i] + dt - dt*h[i]*Vm[i]*0.00005f;
        }
        //swap buffers
        swap(Vm_next,Vm);
        swap(n_next,n);
        swap(m_next,m);
        swap(h_next,h);    
        #endif
    }
    double duration = double(clock()-tick)/CLOCKS_PER_SEC;
    double onestep = duration*double((1000000000.0f/TIMESTEPS)/COMPARTMENTS);
    double bandwidth = 2*4*sizeof(float)*COMPARTMENTS*TIMESTEPS/(1000000000*duration);
    printf("t: %.3f s\n%.3f ns/compartment\n%.3f [GB/s] r/w\n",duration,onestep,bandwidth);

    return 0;
}