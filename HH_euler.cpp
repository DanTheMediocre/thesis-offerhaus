#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <valarray>

using namespace std;

#define COMPARTMENTS 1000000
#define TIMESTEPS   8000

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

float S_m; // Membrane Elastance 1/Cm
float I_m; // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.0f;
const float h_init = 0.5f;

std::vector<float> param {gb_Na,gb_K,gb_l,V_Na,V_K,V_l};
std::vector<float> state {Vm_init,n_init,m_init,h_init};  //{Vm,n,m,h}
std::vector<std::vector<float>> states (COMPARTMENTS,state);

std::vector<float> Vmv (COMPARTMENTS,Vm_init);
std::vector<float> nv (COMPARTMENTS,n_init);
std::vector<float> mv (COMPARTMENTS,m_init);
std::vector<float> hv (COMPARTMENTS,h_init);

std::valarray<float> Vmva (Vm_init,COMPARTMENTS);
std::valarray<float> nva (n_init,COMPARTMENTS);
std::valarray<float> mva (m_init,COMPARTMENTS);
std::valarray<float> hva (h_init,COMPARTMENTS);
std::valarray<float tmp1 (0.0f, COMPARTMENTS);
std::valarray<float tmp2 (0.0f, COMPARTMENTS);

//consider std::valarray<float> for state vector, allows math operands on each element

std::vector<float> nStep(std::vector<float> Vm,std::vector<float> n)
{
    std::vector<float> tmp = {(10.0f-Vm[0])*0.01f};

    return{(1.0f-n[0])*tmp[0]/(exp(10.0f*tmp[0])-1.0f) - n[0]*0.125f*exp(Vm[0]/-80.0f)};
}

std::vector<float> nStep_2(float dt, std::vector<float> Vm,std::vector<float> n)
{
    std::vector<float> tmp1 = Vm;
    std::vector<float> tmp2 = Vm;
    std::for_each(tmp1.begin(),tmp1.end(), [](float &s){s = 0.1f-0.01f*s;});            //tmp1 = (10-Vm)/100
    std::for_each(tmp1.begin(),tmp1.end(), [](float &s){s = s/(exp(10.0f*s)-1.0f);});   //tmp1 = An(Vm) 
    std::for_each(tmp2.begin(),tmp2.end(), [](float &s){s = 0.125f*exp(s/-80.0f);});    //tmp2 = Bn(Vm)
    int i = 0;
    while(i < tmp1.size())
    {
        tmp2[i] += tmp1[i];                                                             //tmp2 = Bn(Vm) + An(Vm)
        tmp1[i] -= tmp2[i]*n[i];                                                        //tmp1 = An(Vm) - n*(Bn(Vm)+An(Vm)) = dn/dt
        tmp1[i] = n[i] + dt*tmp1[i];                                                    //tmp1 = n + dt*(dn/dt)
        i++;                                                           
    }                                                                           
    // n[i] = (1 - n[i]) * A(V) - n[i] * B(V) =  A(V) - (A(V)+B(V))n[i]
    return tmp1;
}

std::valarray<float> nStep_2(float dt, std::valarray<float> Vm,std::valarray<float> n)
{
    std::valarray<float> tmp1 = Vm;
    tmp1 = tmp1.apply([](float &s)->float{return (0.1f-0.01f*s);});

    std::for_each(tmp1.begin(),tmp1.end(), [](float &s){s = 0.1f-0.01f*s;});            //tmp1 = (10-Vm)/100
    std::valarray<float> tmp2 = tmp1;
    std::for_each(tmp1.begin(),tmp1.end(), [](float &s){s = s/(exp(10.0f*s)-1.0f);});   //tmp1 = An(Vm) 
    std::for_each(tmp2.begin(),tmp2.end(), [](float &s){s = 0.125f*exp(s/-80.0f);});    //tmp2 = Bn(Vm)
    int i = 0;
    while(i < tmp1.size())
    {
        tmp2[i] += tmp1[i];                                                             //tmp2 = Bn(Vm) + An(Vm)
        tmp1[i] -= tmp2[i]*n[i];                                                        //tmp1 = An(Vm) - n*(Bn(Vm)+An(Vm)) = dn/dt
        tmp1[i] = n[i] + dt*tmp1[i];                                                    //tmp1 = n + dt*(dn/dt)
        i++;                                                           
    }                                                                           
    // n[i] = (1 - n[i]) * A(V) - n[i] * B(V) =  A(V) - (A(V)+B(V))n[i]
    return tmp1;
}


std::vector<float> mStep(std::vector<float> Vm,std::vector<float> m)
{
    float tmp = 0.1f*(25.0f-Vm[0]);
    return{(1.0f-m[0])*tmp/(exp(tmp)-1.0f) - m[0]*4.0f*exp(Vm[0]/-18.0f)};
}

std::vector<float> mStep_2(float dt, std::vector<float> Vm,std::vector<float> m)
{
    std::vector<float> tmp1 = Vm;
    std::vector<float> tmp2 = Vm;
    std::for_each(tmp1.begin(),tmp1.end(), [](float &s){s = 2.5f-0.1f*s;});             //tmp1 = (25-Vm)/10
    std::for_each(tmp1.begin(),tmp1.end(), [](float &s){s = s/(exp(s)-1.0f);});         //tmp1 = Am(Vm) 
    std::for_each(tmp2.begin(),tmp2.end(), [](float &s){s = 4.0f*exp(s/-18.0f);});      //tmp2 = Bm(Vm)
    int i = 0;
    while(i < tmp1.size())
    {
        tmp2[i] += tmp1[i];                                                             //tmp2 = Bm(Vm) + Am(Vm)
        tmp1[i] -= tmp2[i]*m[i];                                                        //tmp1 = Am(Vm) - m*(Bm(Vm)+Am(Vm))
        tmp1[i] = m[i] + dt*tmp1[i];                                                    //tmp1 = m + dt*(dm/dt)
        i++;                                                                            
    }                                                                           
    // m[i] = (1 - m[i]) * A(V) - m[i] * B(V) =  A(V) - (A(V)+B(V))m[i]
    return tmp1;
}


std::vector<float> hStep(std::vector<float> Vm, std::vector<float> h)
{
    return{(1.0f-h[0])*0.07f*exp(Vm[0]/-20.0f) - h[0]*(1.0f/(1.0f+exp(3.0f - 0.1f*Vm[0])))};
}

std::vector<float> hStep_2(float dt, std::vector<float> Vm,std::vector<float> h)
{
    std::vector<float> tmp1 = Vm;
    std::vector<float> tmp2 = Vm;
    std::for_each(tmp1.begin(),tmp1.end(),[](float &f){f = 0.07f*exp(f/-20.0f);});          //tmp1 = An(Vm) 
    std::for_each(tmp2.begin(),tmp2.end(),[](float &f){f = 1.0f+exp(3.0f-0.1f*f);});        //tmp2 = Bn(Vm)
    int i = 0;
    while(i < tmp1.size())
    {
        tmp2[i] += tmp1[i];                                                                 //tmp2 = Bh(Vm) + Ah(Vm)
        tmp1[i] -= tmp2[i]*h[i];                                                            //tmp1 = Ah(Vm) - h*(Bh(Vm)+Ah(Vm))
        tmp1[i] = h[i] + dt*tmp1[i];                                                        //tmp1 = h + dt*(dh/dt)
        i++;                                                                            
    }                                                                           
    // h[i] = (1 - h[i]) * A(V) - h[i] * B(V) =  A(V) - (A(V)+B(V))h[i]
    return tmp1;
}

std::vector<float> VmStep_2(float dt, std::vector<float> Vm, std::vector<float> n, std::vector<float> m, std::vector<float> h)
{
    std::vector<float> tmp1 = Vm;
    std::for_each(tmp1.begin(),tmp1.end(),[=](float &f){f += dt*S_m*I_m;});      //tmp1 = Vm + dt*(Im/Cm)
    for(int i = 0;i < tmp1.size(); i++)
    {
        tmp1[i] -= dt*S_m*gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na);               //tmp1 = Vm + dt*1/Cm*(Im - gb_na*n^4*(Vm-Vna))
    }
    for(int i = 0;i < tmp1.size(); i++)
    {
        tmp1[i] -= dt*S_m*gb_K*m[i]*m[i]*m[i]*h[i]*(Vm[i]-V_K);                 //tmp1 = Vm + dt*(1/Cm)*(Im - gb_na*n^4*(Vm-Vna) - gb_K*h*m^3*(Vm-VK))
    }
    for(int i = 0;i < tmp1.size(); i++)
    {
        tmp1[i] -= dt*S_m*gb_l*(Vm[i]-V_l);                                     //tmp1 = Vm + dt*(1/Cm)*(Im - gb_na*n^4*(Vm-Vna) - gb_K*h*m^3*(Vm-VK) - gb_l*(Vm-Vl))
    }
    return tmp1;                                                                //tmp1 = Vm + dt*(dVm/dt)
}

float VmStep( std::vector<float> state)
{
    return S_m*(I_m - gb_Na*powf(state[1],4)*(state[0]-V_Na)
                    - gb_K*state[3]*powf(state[2],3)*(state[0]-V_K)
                    - gb_l*(state[0]-V_l));
}

void fwd_euler(float t_0, float t_end, float tstep, std::vector<float>& state)
{
    while(t_0<t_end)
    {
        t_0 += tstep;
        state[0] = state[0] + tstep * VmStep(state);
        state[1] = state[1] + tstep * nStep({state[0]},{state[1]}).front();
        state[2] = state[2] + tstep * mStep({state[0]},{state[2]}).front();
        state[3] = state[3] + tstep * hStep({state[0]},{state[3]}).front();
    }
}

void fwd_euler_2(float t_0, float t_end, float tstep, std::vector<float> Vm, std::vector<float> n, std::vector<float> m, std::vector<float> h)
{
    while(t_0<t_end)
    {
        t_0 += tstep;
        Vm = VmStep_2(tstep,Vm,n,m,h);
        n = nStep_2(tstep,Vm,n);
        m = mStep_2(tstep,Vm,m);
        h = hStep_2(tstep,Vm,h);
    }
}

void fwd_euler_3(float dt,  std::valarray<float>& Vm,
                            std::valarray<float>& n,
                            std::valarray<float>& m,
                            std::valarray<float>& h,
                            std::valarray<float>& tmp1, 
                            std::valarray<float>& tmp2)
{
    for(int i = 0; i < Vm.size();Vm++)
    {
        
    }
}



int main()
{
    float v0 = 0.1f;   //initial Membrane Potential Vm,t0[V]
    float dt = 1.0/TIMESTEPS; //timestep t_delta [S]
    float t_0 = 0.0f;
    const float t_end = 1.0f;
    double diff_tick = clock();
    double tstart = diff_tick;
    int n = 10;
    S_m = 100.0f;   // 1mF
    I_m = 0.5f;
    // for(int j=0;j<n;j++)
    // {
    //     diff_tick = clock();
    //     for(int i=j;i < j+1000;i++)
    //     {
    //         state = states[i];
    //         *state.begin()=v0;
    //         fwd_euler(0.0,1.0,dt,state);
    //         states[i] = state;
    //     }
    //     std::cout << j+1 << "/"<< n << "\t" <<(clock()-diff_tick) << endl;
    // }
    std::cout << "0/" << TIMESTEPS << "\t0" << endl;
    for(int t = 0; t < TIMESTEPS; t++)
    {
        diff_tick = clock();
        Vmv = VmStep_2(dt,Vmv,nv,mv,hv);
        nv = nStep_2(dt,Vmv,nv);
        mv = mStep_2(dt,Vmv,mv);
        hv = hStep_2(dt,Vmv,hv);
        std::cout << t << "/"<< TIMESTEPS << "\t" <<(clock()-diff_tick) << endl;
    }
    std::cout << "t: " << (clock()-tstart) << endl;
    return 0;
}