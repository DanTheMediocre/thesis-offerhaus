#include <cmath>
#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <valarray>
// #include "omp.h"

using namespace std;

#define COMPARTMENTS 1000000
#define TIMESTEPS   10

const float V_K = -0.077f;
const float V_Na = 0.055f;
const float V_l = -0.065f;
const float gb_Na = 0.040f;
const float gb_K = 0.035f;
const float gb_l = 0.0003f;

const float S_m = 1000.0f;    // Membrane Elastance 1/Cm
const float I_m = 0.1f;     // Transmembrane Current

const float Vm_init = 0.1f;
const float n_init = 1.0f;
const float m_init = 0.0f;
const float h_init = 0.5f;

const float dt = 0.000125f;

float dV_Na, dV_K, dV_l, dV_dt, V_tmp, rateA, rateB; //private
std::vector<float> Vm (COMPARTMENTS,Vm_init);
std::vector<float> n (COMPARTMENTS,n_init);
std::vector<float> m (COMPARTMENTS,m_init);
std::vector<float> h (COMPARTMENTS,h_init);

void hh_step_all_compartments(std::vector<float>& Vm, std::vector<float>& n, std::vector<float>& m, std::vector<float> h)
{
        for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
        {
            Vm[i] += dt*S_m*(I_m - gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - gb_l*(Vm[i]-V_l));
            n[i] += dt*((1.0f - n[i])*(0.01f*(10.0f-Vm[i])/(expf(0.1f*(10.0f-Vm[i]))-1.0f)) - n[i]*(0.125f*expf(Vm[i]*-0.0125f)));
            m[i] += dt*((1.0f - m[i])*(0.1f*(25.0f-Vm[i])/expf(0.1f*(25.0f-Vm[i]))) - m[i]*(4.0f*expf(Vm[i]/-18.0f)));
            h[i] += dt*((1.0f - h[i])*(0.07f*expf(Vm[i]*-0.05f)) - h[i]*(1.0f/(1.0f+expf(0.1f*(30.0f-Vm[i])))));
        }
}

void hh_step_all_compartments_v2(std::vector<float>& Vm, std::vector<float>& n, std::vector<float>& m, std::vector<float> h)
{
        for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
        {
            //Vm[i] += dt*S_m*(I_m - gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - gb_l*(Vm[i]-V_l));
            {
            dV_Na = S_m*gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na);
            dV_K = S_m*gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K);
            dV_l = S_m*gb_l*(Vm[i]-V_l);
            dV_dt = S_m*I_m - dV_Na - dV_K - dV_l;
            Vm[i] += dt*dV_dt;
            }
            // n[i] += dt*((1.0f - n[i])*(0.01f*(10.0f-Vm[i])/(expf(0.1f*(10.0f-Vm[i]))-1.0f)) - n[i]*(0.125f*expf(Vm[i]*-0.0125f)));
            {
            V_tmp = 0.1f*(10.0f-Vm[i]);
            rateA = 0.1f*V_tmp/(expf(V_tmp)-1.0f);
            rateB = 0.125f*expf(Vm[i]*-0.0125f);
            n[i] += dt - dt*(rateA+rateB)*n[i];
            }
            m[i] += dt*((1.0f - m[i])*(0.1f*(25.0f-Vm[i])/expf(0.1f*(25.0f-Vm[i]))) - m[i]*(4.0f*expf(Vm[i]/-18.0f)));
            h[i] += dt*((1.0f - h[i])*(0.07f*expf(Vm[i]*-0.05f)) - h[i]*(1.0f/(1.0f+expf(0.1f*(30.0f-Vm[i])))));
        }
}

int main(void)
{
    // double clockdiff = clock();
    for(int t = 0;t < TIMESTEPS; t++)   //loop timesteps
    {
        // clockdiff = clock();
        // #pragma omp parallel for
        for(int i = 0;i < COMPARTMENTS;i++)   //loop compartments
        {
            Vm[i] += dt*S_m*(I_m - gb_Na*n[i]*n[i]*n[i]*n[i]*(Vm[i]-V_Na) - gb_K*h[i]*m[i]*m[i]*m[i]*(Vm[i]-V_K) - gb_l*(Vm[i]-V_l));
            n[i] += dt*((1.0f - n[i])*(0.01f*(10.0f-Vm[i])/(expf(0.1f*(10.0f-Vm[i]))-1.0f)) - n[i]*(0.125f*expf(Vm[i]*-0.0125f)));
            m[i] += dt*((1.0f - m[i])*(0.1f*(25.0f-Vm[i])/expf(0.1f*(25.0f-Vm[i]))) - m[i]*(4.0f*expf(Vm[i]/-18.0f)));
            h[i] += dt*((1.0f - h[i])*(0.07f*expf(Vm[i]*-0.05f)) - h[i]*(1.0f/(1.0f+expf(0.1f*(30.0f-Vm[i])))));
        }
        // std::cout << "t:" << ((clock()-clockdiff) * 1000 / CLOCKS_PER_SEC) << " ms" << endl;
        // // hh_step_all_compartments(Vm,n,m,h);
    }
}